    $(document).ready(function($) {

        //TABS PARA SEMANAS
        $('#semanas').easytabs();


	/*--slider bottom--*/
	$('.slider_prod').bxSlider({
	  minSlides		: 3,
	  maxSlides		: 4,
	  slideWidth	: 222,
	  slideMargin	: 10,
	  pager			: false
	});

    /*--slider o Carrousel Semanas--*/
  $('.total_semanas').bxSlider({
    minSlides   : 9,
    maxSlides   : 9,
    slideWidth  : 25,
    slideMargin : 10,
    moveSlides  : 8,
    pager     : false,
    infiniteLoop: false,
    auto : false
  });

	/* ----- BANNER SLIDE PPAL ------ */

	$('#bfade-home-panales').bjqs({
	  height      : 470,
	  width       : '100%',
	  responsive  : true,
	  automatic : true,
	  showcontrols : true, // show next and prev controls
	  centercontrols : true, // center controls verically
	  nexttext : 'Next', // Text for 'next' button (can use HTML)
	  prevtext : 'Prev', // Text for 'previous' button (can use HTML)
	  showmarkers : false, // Show individual slide markers
	  centermarkers : false, // Center markers horizontally
	});
	/* ----- EOFF BANNER SLIDE PPAL ------ */

  // EOFF Funcionalidad del formulario de login

  // WINNY EMBARAZADAS

  $('#embarazo-tab-container').easytabs();

  $('.slider_tab').bxSlider({
    minSlides   : 3,
    maxSlides   : 4,
    slideWidth  : 175,
    slideMargin : 0,
    pager     : false
  });

  $('.mimundo-tab').bxSlider({
    minSlides   : 3,
    maxSlides   : 3,
    pager     : false,
  });

  $('#embarazo-tab-container .etabs .tab a').click(function (){
    $(this).addClass('active');
  });

  // EOFF WINNY EMBARAZADAS

});
