$(document).ready(function($) {

//FUNCION PARA DESPLEGAR SEMANA CALCULADA*****************************************

  $('#calcular_semana').click(function (){
    $("#resultado_calcular_semana").slideToggle(400);
    return false;
  });

  /****/

  $('.tooltip').tooltipster({
      contentAsHTML: true
  });

//FUNCION PARA DESPLEGAR COMENTARIOS*************************************************

  $('#comentar_articulo').click(function (){
    $("#desplegar_panel_comentarios").slideToggle(400);
    return false;
  });

//FUNCION PARA DESPLEGAR PERFIL DE CADA MEDICO***************************************

  $('#ver-perfil').click(function (){
    $("#perfil-especialista").slideToggle(400);
    return false;
  });

//FUNCION PARA DESPLEGAR BUSQUEDA AVANZADA*******************************************

  $('#busqAvanzada').click(function (){
    $("#bloqBusqAvanzada").slideToggle(400);
    return false;
  });

//FUNCION PARA TOOGLE DE LOS NOMBRES**************************************************

  $('.contentNombres ul.contenido li a').click(function(){
    var nombre = $(this).attr('data-lista');
    $('.contentNombres ul.contenido li article.contenidos').slideUp('fast');
    $('.contentNombres ul.contenido li a').removeClass('active');
    if ($('.contentNombres ul.contenido li#'+nombre+' article.contenidos').is (':hidden')){
      $('.contentNombres ul.contenido li#'+nombre+' article.contenidos').slideToggle('fast');
      $('.contentNombres ul.contenido li#'+nombre+' a').addClass('active');
    }
    return false;
  });

//FUNCION PARA FILTROS DE BUSQUEDA BIBLIOTECA*****************************************

  $('ul.filtrosTop li a.top').click(function(){
    var filtro = $(this).attr('href');
    $('ul.filtrosTop li ul.contentHidden').slideUp('fast');
    if ($('ul.filtrosTop li ul'+filtro).is (':hidden')){
      $('ul.filtrosTop li ul'+filtro).slideToggle('fast').mouseleave(function(){
        $('ul.filtrosTop li ul'+filtro).slideUp('fast');
      });
    };
    return false;
  });

//AGREGAR Y QUITAR CLASES FILTRO BIBLIOTECA********************************************
$('.addClass').click(function(){
  $('.addClass').removeClass('active');
  $(this).addClass('active');
  return false;
});

//TABS

  $('#tab-container, #menusWinny, #tabNombresAZ, #semana-semana, #dias .menuDiario, #semanas').easytabs();

    /*--slider o Carrousel Semanas--*/
  $('.total_semanas_sas').bxSlider({
    minSlides   : 10,
    maxSlides   : 50,
    slideWidth  : 25,
    slideMargin : 10,
    moveSlides  : 2,
    pager     : false,
    infiniteLoop: false,
    auto : false
  });

  /* SLIDER ZODIACO */

  $('.signos_zodiaco').bxSlider({
    slideWidth: 96,
    minSlides: 4,
    maxSlides: 12,
    slideMargin: 5,
    adaptiveHeight: false,
    pager     : false,
    moveSlides  : 1
  });

  var sliderRecetas = $('.recetasMenu').bxSlider({
    minSlides   : 3,
    maxSlides   : 3,
    slideWidth  : 155,
    slideMargin : 20,
    pager     : false,
    infiniteLoop: false
  });

  /*
  * Funcion para reestructurar calendario
  */

  var initial_calendar = 0;
  var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

  $('#calcular').click(function () {

    $('#calendario-ovulacion').slideToggle("fast", function () {

      if (initial_calendar == 0){

          $('#calendar').delay(600).fullCalendar({
          monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
          dayNamesShort: ['Dom','Lun','Mar','Mie','Jue','Vie','Sab'],
          header: {
            left: 'prev',
            center: 'title',
            right: 'next'
          },
          buttonText: {
            prev: "Mes Anterior",
            next: "Mes Siguiente"
          },
          editable: false,
          events: [
            {
              title: '',
              start: new Date(y, m, 4),
              className: 'test'
            },
            {
              title: '6',
              start: new Date(y, m, 6),
              className: 'ciclo'
            },
            {
              title: '',
              start: new Date(y, m, 19),
              className: 'fertil'
            },
            {
              title: '',
              start: new Date(y, m, 20),
              className: 'fertil'
            },
            {
              title: '',
              start: new Date(y, m, 21),
              className: 'muy-fertil'
            },
            {
              title: '',
              start: new Date(y, m, 22),
              className: 'muy-fertil'
            },
            {
              title: '',
              start: new Date(y, m, 23),
              className: 'ovulacion'
            },
            {
              title: '',
              start: new Date(y, m, 27),
              className: 'anidacion'
            },
            {
              title: '',
              start: new Date(y, m, 28),
              className: 'anidacion'
            }]
        });
        initial_calendar = 1;
      };
  });

  });

/* FUNCION PARA EL SCROLLBAR SLIDER MIMUNDOWINNY */

  $("section.mwcontetnSlider").mCustomScrollbar({
      horizontalScroll:true,
      autoDraggerLength: true,
      autoHideScrollbar: false,
      contentTouchScroll: true,
      theme:"dark"
  });

//SLIDER PARA LOS TIPS WITH TUMBNAILS

  $('.sliderTip').bxSlider({
    pagerCustom: '#sliderTipPager',
    responsive: false,
    mode: 'horizontal',
    nextText: '',
    prevText: ''
  });

//FUNCIONALDIAD MENU WINNY*******************************************
  var valor = $("#tipoMenu").val();
  $("#generarMenusDias").attr("href", "#"+valor);
  $( "#tipoMenu" ).change(function () {
    valor = $(this).val();
    $("#generarMenusDias").attr("href", "#"+valor);
  });
  $("#generarMenusDias").click(function(){
      var seleccionFinal = $(this).attr("href");
      if ($("#dias").is(":hidden") && $("#semanas").is(":hidden")) {
        $(seleccionFinal).fadeIn('fast');
      } else {
        if ($(seleccionFinal).is(":hidden")) {
          $(".menuPresentacion").fadeOut('fast',function(){
            $(seleccionFinal).fadeIn('fast');
          });
        };
      };
      return false;
  });
  $('#menusWinny ul.etabs li.tab a').click(function(){
    if ($('article.menuPresentacion').is(':visible')) {
        $("article.menuPresentacion").fadeOut('fast');
    } else {
        if ($('article.recetasRecomendadas').is(':visible')) {
            $("article.recetasRecomendadas").fadeOut('fast');
            $("article.contenidosRecetasInternas").fadeOut('fast');
        }
    }
    return false;
  });
  $('#generarTodasRecetasRecomend').click(function(){
    /** Función Acordeon para ingredientes de Winny Menu  **/
    if ($('#TodasrecetasRecomend').is(':hidden')){

        $('#TodasrecetasRecomend').slideDown('slow');
        $('#generarTodasRecetasRecomend span').text( "Volver" );
        $('#recetasRecomend').slideUp('slow');
        $('.ingredientes').slideUp('slow');
        $('.pagination--recets').slideUp('slow');
        $('.pagination--allrecets').slideDown('slow');
        $('#generarRecetasRecomend').slideUp('fast');

    } else {

        $('#recetasRecomend').slideUp('fast');
        $('.contenidosRecetasInternas').slideUp('fast');
        $('.ingredientes').slideDown('slow');
        $('.pagination--recets').slideDown('slow');
        $('.pagination--allrecets').slideUp('slow');
        $('#TodasrecetasRecomend').slideUp('slow');
        $('#generarRecetasRecomend').slideDown('fast');
        $('#generarTodasRecetasRecomend span').text( "Ver todas las recetas" );

    } ;
  });

  $('#generarRecetasRecomend').click(function(){
    sliderRecetas.reloadSlider();
    /** Función Acordeon para ingredientes de Winny Menu  **/
    if ($('#recetasRecomend').is(':hidden') ){
        $('#recetasRecomend').slideDown('slow');
        $('.ingredientes').slideUp('slow');
        $('.pagination--recets').slideUp('slow');
        $('#generarRecetasRecomend span').text( "Buscar de nuevo" );
        $('#TodasrecetasRecomend').slideUp('slow');
      $('#generarTodasRecetasRecomend').slideUp('fast');
    } else {
      $('.ingredientes').slideDown('slow');
      $('.pagination--recets').slideDown('slow');
      $('#recetasRecomend').slideUp('slow');
      $('.contenidosRecetasInternas').slideUp('slow');
        $('#generarTodasRecetasRecomend').slideDown('fast');
      $('#generarRecetasRecomend span').text( "Generar Receta" );
    };
  });
  $('ul.recetasMenu li div.pan-button a.leer-vinculo, ul.TodasrecetasMenu li div.pan-button a.leer-vinculo').click(function(){
        var recetaId = $(this).attr("data-receta");
        if ($(recetaId).is(':hidden')) {
            $(".contenidosRecetasInternas").fadeOut('fast');
            $(recetaId).fadeIn('fast');
        };
        return false;
  });

//FUNCIONALIDAD DÍAS GALLERY******************************************

  var total_listas = $("#dias ul.diasDiario").children().length;
  var listas_visibles = 11;
  if (total_listas <= 11){
    $('#btn_right').hidden;
    $('#btn_left').hidden;
  }
  $("#btn_right").click(function(){
      if(listas_visibles < total_listas){
        listas_visibles = listas_visibles+3;
        $("#dias ul.diasDiario").animate({"left": "-=166px"},"slow");
        $("#btn_left").fadeIn('slow');
      }
      if(listas_visibles >= total_listas){
        $(this).fadeOut('slow');
      }
  });
  $("#btn_left").click(function(){
    if(listas_visibles > 11){
      listas_visibles = listas_visibles-3;
      $("#dias ul.diasDiario").animate({"margin-left": "+=166px"},"slow");
      $("#btn_right").fadeIn('slow');
    }
      if(listas_visibles <= 11){
        $(this).fadeOut('slow');
      }
  });


//FUNCIONALIDAD BOTON AGREGAR NUEVO BEBE******************************

    $('.newBabe').click(function(){
        var state = $(this).attr('data-state');
        if (state == "inactive") {
            $('#nuevoBebe').slideDown( "fast" );
            $(this).attr('data-state','active');
            $(this).addClass('active');
        } else if (state == "active") {
            $('#nuevoBebe').slideUp( "fast" );
            $(this).attr('data-state','inactive');
            $(this).removeClass('active');
        };
        return false;
    });

//OBTENER POSICION DE UN ELEMENTO RESPECTO A SU PADRE*****************

  // function getPosition(element) {
  //   var xPosition = 0;
  //   var yPosition = 0;

  //   while(element) {
  //       xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
  //       yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
  //       element = element.offsetParent;
  //   }
  //   return { x: xPosition, y: yPosition };
  // };

  // var position = getPosition(myElement);
  // console.log(position.x + " - " + position.y);

  function getPosition (element){
    var childPos = $( element ).offset();
    var parentPos = $( element ).parent().offset();
    var childOffset = {
      top: childPos.top - parentPos.top,
      left: childPos.left - parentPos.left
    }
    return { x: childOffset.left, y: childOffset.top };
  }

//FUNCIONALIDAD SUBMENU WINNY SHOWER**********************************

$('#winnyShower').click(function(e){
  e.preventDefault();
  $('ul.listMenu ul.subMenu').slideToggle('fast');
});

$('.calendarioPDFmenu').click(function(e){
  e.preventDefault();
  $('#calendarioPDFsubmenu').slideToggle('fast');
});

//DRAG ELEMENTOS BARRA ESTADO******************************************

  $(function() {

    var position = '';
    var myElement = '';
    var widthBar = $("#barraEtapas").width();
    var valMes = parseFloat(widthBar / 36);
    var meses = 0;
    $("#barraEtapas a").click(function(e){e.preventDefault();});
    $( "#dragInit" ).draggable({
      containment: "#barraEtapas",
      scroll: false,
      axis: "x",
      start: function() {
        myElement = document.querySelector("#dragInit");
        $(this).addClass('clicked');
      },
      drag: function() {
        position = getPosition (myElement).x;
        console.log(position);
        if (position > 667) {
          $("#dragInit").draggable( 'option',  'revert', true );
          $("#dragInit").css('margin-top','-2px');
        } else {
          $("#dragInit").draggable( 'option',  'revert', false );
          $("#dragInit").css('margin-top','-2px');
        };
      },
      stop: function() {
        // position = parseInt(getPosition (myElement).x) + 9.5;
        for (var i = 0; i <= 684;) {
          if (position >= i && position < (i+19)){
            if (position >= 684) {
              meses = 36;
            } else {
              meses = (i+19)/19;
            };
          };
          i+=19;
        };
        $(this).html('<p>'+meses+'<br>Meses</p>');
        $(this).removeClass('clicked');
      }
    });
    $( "#dragEnd" ).draggable({
      containment: "#barraEtapas",
      scroll: false,
      axis: "x",
      start: function() {
        myElement = document.querySelector("#dragEnd");
        $(this).addClass('clicked');
      },
      drag: function() {
        position = getPosition (myElement).x;
        console.log(position);
        if (position > 667) {
          $("#dragEnd").draggable( 'option',  'revert', true );
          $("#dragEnd").css('margin-top','-2px');
        } else {
          $("#dragEnd").draggable( 'option',  'revert', false );
          $("#dragEnd").css('margin-top','-2px');
        };
      },
      stop: function() {
        // position = parseInt(getPosition (myElement).x) + 9.5;
        for (var i = 0; i <= 684;) {
          if (position >= i && position < (i+19)){
            if (position >= 684) {
              meses = 36;
            } else {
              meses = (i+19)/19;
            };
          };
          i+=19;
        };
        $(this).html('<p>'+meses+'<br>Meses</p>');
        $(this).removeClass('clicked');
      }
    });
  });

  /***********************SLIDER RANGE*************************/

  $(function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 15,
      values: [ 0, 3 ], //Rango inicial, o Rango seleccionado por el usuario***
      slide: function( event, ui ) {
        $( "#valorMeses" ).text( ui.values[ 0 ] + " - " + (ui.values[ 1 ]) );
      }
    });

    //$( "#slider-range" ).slider( "disable" ); //Desactivar el slider, rango de meses**
    $( "#slider-range" ).slider( "enable" ); //Activar el slider, rango de meses**
  });

  /**************Active Menu Baby Shower***************/

  if( $('body').hasClass('js-baby-shower-1') )
  {
    $('ul.subMenu').css('display', 'block');
  }

});


var functions = {

  insertGift : function (idGuest) {
    $('#'+idGuest).append( '<br><div class="row-fluid"><div class="span12"><input type="text"/></div></div>' );
    $('.btn-register-gifts').css('display', 'block');
  },


}
