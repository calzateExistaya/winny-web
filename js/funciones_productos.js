    $(document).ready(function($) {

/*---------FUNCIONALIDAD DEL TOOLTIP--------------*/

    $('.disenos_panales').tooltipster({
        theme: '.my-custom-theme',
        animation: 'grow',
        interactiveTolerance:1,
        speed: 100,
        offsetY: -25,
        offsetX: -3,
        position: 'top',
        fixedWidth: 231
    });

/*-------FUNCIONE PARA DESPLEGAR EL MENU DE OTROS PRODUCTOS----------*/

  $('#click-aqui-btn').click(function (){
    var texto = $(this).text();
    $(this).html('Cerrar');
      if (texto == 'Cerrar'){
          $(this).html('Clic Aquí');
      }
    console.log (texto);
    $("#otros_productos_content").slideFadeToggle(400);
  });

//FUNCION PARA MOSTRAR LOS CONTENIDOS DE LOS TABS******************************

  $('#link_inicio').click(function (){

      $("#item1").slideToggle('slow');

  });

    $('#link_inicio').click(function (){

      $("#item2").slideToggle('slow');

  });

	// Funcionalidad Menu Lateral
	$('#left-menu-conte .accordion .accordion-group .accordion-heading a').click(function(){
	  $('.infozone').hide();

    var region = $(this).attr('data-region');
	  $('#' + region).fadeIn(580);
	  for(i=1;i<=13;i++)
	  {
	  	$('#left-menu-conte .accordion .accordion-group .accordion-heading a').removeClass('nav-'+i);
		$('.subm-sobre-winny ul li a').removeClass('nav-'+i);
		$('.subm-winny-social ul li a').removeClass('nav-'+i);
	  }
	  
	  $(this).addClass(region);
 
    });

    $('.bjqs-markers li a').click(function(){
		// Slide a mostrar
		var slideToShow = $(this).attr('slide-to-show');
		// oculta los demas slides
		$('#slide_inst ul.bjqs li').hide();
		// quita la clase que la coloca como activa
		$('.subm-sobre-winny ul li a').removeClass('nav-3');
		//coloca como activo esta op en el menu lateral
		$(this).addClass('nav-3');
		//sobre_winny01
		$('#sobre_winny0' + slideToShow).fadeIn(580);
  	});
	
	/* ------ PRODUCTOS ------- */

	/*--slider bottom--*/

	$('.slider_prod').bxSlider({
	  minSlides		: 3,
	  maxSlides		: 4,
	  slideWidth	: 222,
	  slideMargin	: 10,
	  pager			: false
	});
	
	/* Menu interno de las etapas (corazones) */
	$('#products-continer div div.menu-etapas-inner ul li a').click(function(){
		$('.product-info').hide();
		
		var prod_region = $(this).attr('data-region');
		
		$('#' + prod_region).fadeIn(580);
		
		$('#products-continer div div.menu-etapas-inner ul li a').css('color', '#92AECF');
		$('#products-continer div div.menu-etapas-inner ul li a').removeClass('selected');
		$(this).addClass('selected'); 
		//$(this).css('color', '#FFF');

    return false;
	
	});
	
	/* puntos de referencia del producto pañal o toallita, etc*/
	$('.ref-point').mouseenter(function(){
		$('.product-properties').hide();
		var prod_property = $(this).attr('area-reference');
		$('#' + prod_property).fadeIn(580);
	});
	
	$(".product-properties").mouseleave(function() {
		$(this).fadeOut();
    });


	/*--TABS GENERALES PERSENTACION POR PESO Y POR ESTAPAS--*/

	$('#peso-etapa-tab-container').easytabs();

	/*--TABS PARA LA NAVEGACION POR ETAPAS--*/

	$('#peso-etapa-tab-container #presentacion-etapas ul li a').click(function(){
    // Slide a mostrar
    var slideToShow = $(this).attr('slide-to-show');
    // oculta los demas slides
    $('#peso-etapa-tab-container .content-etapas div').hide();
    // quita la clase que la coloca como activa
    $('#peso-etapa-tab-container #presentacion-etapas ul li a').removeClass('selected');
    //coloca como activo esta op en el menu lateral
    $(this).addClass('selected');
    //sobre_winny01
    $('#peso-etapa-tab-container #presentacion_etapa' + slideToShow).fadeIn(580);
  });

	/*--SLIDE PARA LA NAVEGACION POR PESO--*/

  	slider_peso = $('.slide_peso_etapas').bxSlider({
  		mode			: 'fade',
  		maxSlides		: 1,
	  	pager			: false,
	  	speed			: 100
  	});

	var slidePhotoShowed = false;

    $('#peso-etapa-tab-container ul.etabs li a').click(function(){
      var tab = $(this).attr('href');
  			if(tab == '#peso-tab'){
  				if(!slidePhotoShowed)
  				{
  					$('#peso-etapa-tab-container').bind('easytabs:after', function(evt, tab, panel, data) {
    						if ( panel.hasClass('active') ) {
  							slider_peso.reloadSlider();
  						}
  					});
  					slidePhotoShowed = true;
  				}
  			}
    });
      
      $('#bfade-home-panales').bjqs({
          height      : 470,
          width       : '100%',
          responsive  : true,
          automatic : true,
          showcontrols : true, // show next and prev controls
          centercontrols : true, // center controls verically
          nexttext : 'Next', // Text for 'next' button (can use HTML)
          prevtext : 'Prev', // Text for 'previous' button (can use HTML)
          showmarkers : false, // Show individual slide markers
          centermarkers : false, // Center markers horizontally
        });

      //Paneles de los corazones del home de productos izq
      $('#home-products-panel-left ul li a').click(function(){
        $('#home-products-panel-left .product-photo-info').hide();
        var prod_region_to_show = $(this).attr('product-to-show');
        $('#' + prod_region_to_show).fadeIn(580);
        $('#home-products-panel-left ul li a').removeClass('selected');
        $(this).addClass('selected');
      });

      $('#home-products-panel-center ul li a').click(function(){
        $('#home-products-panel-center .product-photo-info').hide();
        var prod_region_to_show = $(this).attr('product-to-show');
        $('#' + prod_region_to_show).fadeIn(580);
        $('#home-products-panel-center ul li a').removeClass('selected');
        $(this).addClass('selected');
      });

      $('#home-products-panel-right ul li a').click(function(){
        $('#home-products-panel-right .product-photo-info').hide();
        var prod_region_to_show = $(this).attr('product-to-show');
        $('#' + prod_region_to_show).fadeIn(580);
        $('#home-products-panel-right ul li a').removeClass('selected');
        $(this).addClass('selected');
      });     
      /* ----- EOFF HOME PANALES ------ */
   
		/* puntos de referencia del producto pañal o toallita, etc*/
		$('.ref-point-toallitas').mouseenter(function(){
			$('.product-properties-ifo-tapa').hide();
			var prod_property = $(this).attr('area-reference');
			$('#' + prod_property).fadeIn(300);
		}).mouseleave(function() {
			var prod_property = $(this).attr('area-reference');
			$('#' + prod_property).fadeOut();
			$('#info-toallita').fadeIn(300);
		});

});

/*----------FUNCION PARA CAMBIAR LA PRESENTACION DE LOS PAÑALES-------------*/

function cambiar_imagen(id, ruta_img){
	$(document).ready(function($) {
		//$('#'+id).fadeOut().attr('src', '');
		$('#'+id).fadeIn('fast').attr('src', ruta_img);
  });  
}

