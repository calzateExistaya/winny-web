$(document).ready(function($) {


//FUNCIONALIDAD DE SLIDE WINNY SOCIAL*************************************

            slider_one = $('.w_social').bxSlider({
              slideWidth: 120,
              minSlides: 6,
              maxSlides: 6,
              slideMargin: 2,
              adaptiveHeight: false
            });

            slider_two = $('.draclown_2012').bxSlider({
              slideWidth: 120,
              minSlides: 6,
              maxSlides: 6,
              slideMargin: 2,
              adaptiveHeight: false,
            });

            slider_three = $('.draclown_2013').bxSlider({
              slideWidth: 120,
              minSlides: 6,
              maxSlides: 6,
              slideMargin: 2,
              adaptiveHeight: false,
            });

//CONTENIDO PARA EL SLIDE PRINCIPAL DE MI MUNDO WINNY********************

          $('#slide_inst').bjqs({
            height      : 700,
            width       : '100%',
            responsive  : true,
            automatic : false,
            showcontrols : true, // show next and prev controls
      			centercontrols : true, // center controls verically
      			nexttext : 'Next', // Text for 'next' button (can use HTML)
      			prevtext : 'Prev', // Text for 'previous' button (can use HTML)
      			showmarkers : false, // Show individual slide markers
      			centermarkers : false, // Center markers horizontally
          });

//FUNCION PARA MOSTRAR LOS CONTENIDOS DE LOS TABS******************************

  $('#link_inicio').click(function (){
      $("#item1").slideToggle('slow');
  });
    $('#link_inicio').click(function (){
      $("#item2").slideToggle('slow');
  });

	// Funcionalidad Menu Lateral*************************************************

	$('#left-menu-conte .accordion .accordion-group .accordion-heading a').click(function(){
	  $('.infozone').hide();
    var region = $(this).attr('data-region');
	  $('#' + region).fadeIn(580);
	  for(i=1;i<=13;i++)
	  {
	  	$('#left-menu-conte .accordion .accordion-group .accordion-heading a').removeClass('nav-'+i);
		$('.subm-sobre-winny ul li a').removeClass('nav-'+i);
		$('.subm-winny-social ul li a').removeClass('nav-'+i);
	  }
	  $(this).addClass(region);
    });

  //MENU SOBRE WINNY**********************************************************

    $('#collapseOne .subm-sobre-winny .bjqs-markers li a').click(function(){
    // Slide a mostrar
    var slideToShow = $(this).attr('slide-to-show');
    // oculta los demas slides
    $('#slide_inst ul.bjqs li').hide();
    // quita la clase que la coloca como activa
    $('.subm-sobre-winny ul li a').removeClass('nav-3');
    //coloca como activo esta op en el menu lateral
    $(this).addClass('nav-3');
    //sobre_winny01
    $('#sobre_winny0' + slideToShow).fadeIn(580);
    });

  //MENU SOBRE WINNY***********************************************************

    $('#collapseTwo .subm-winny-social .bjqs-markers li a').click(function(){
    // Slide a mostrar
    var slideToShow_ws = $(this).attr('slide-to-show');
    // oculta los demas slides
    $('#nav-3, #nav-7').hide();
    // quita la clase que la coloca como activa
    $('#collapseTwo .subm-winny-social ul li a').removeClass('nav-4');
    //coloca como activo esta op en el menu lateral
    $(this).addClass('nav-4');
    //winny
    $('#' + slideToShow_ws).fadeIn(580);
    //recargar slider donaciones-----------
          slider_one.reloadSlider();
    });

    //Recargar sliders de Dra Clown 2012

    $('.accordion-heading a.menu3').click(function(){
        var chekDisplay = $('#ano_2013').css("display");
        if(chekDisplay == 'block'){
            slider_three.reloadSlider();
        };
    });

	var slidePhotoShowed = false;

    $('#tab-draclown ul.etabs li.tab a').click(function(){
      var tab = $(this).attr('href');
  			if(tab == '#ano_2012'){
  				if(!slidePhotoShowed)
  				{
  					$('#tab-draclown').bind('easytabs:after', function(evt, tab, panel, data) {
    						if ( panel.hasClass('active') ) {
  							slider_two.reloadSlider();
  						}
  					});
  					slidePhotoShowed = true;
  				}
  			}
  			if(tab == '#ano_2013'){
  				if(!slidePhotoShowed){
  					$('#tab-draclown').bind('easytabs:after', function(evt, tab, panel, data) {
    						if ( panel.hasClass('active') ) {
  							slider_three.reloadSlider();
  						}
  					});
  					slidePhotoShowed = true;
  				}
  			}
    });

//FUNCIONALIDAD DEL POP UP***************************************************

    $(".fancybox-button").fancybox({
        prevEffect    : 'fade',
        nextEffect    : 'fade',
        loop          : false,
        closeBtn      : true,
        helpers       : {
        title         : { type : 'inside' },
        buttons       : {},
        overlay       : {
        css           : {'background' : 'rgba(122, 128, 187, 0.85)'}
        }
      }
    });

//CAMBIAR BACKGROUND PQRS BODY****************************************************

      $("a.menu5").click(function () {
        $("body").css("background","#FEF2CC");
      });
      $("a.menu1, a.menu2, a.menu3, a.menu6").click(function () {
        $("body").css("background","#FFF");
      });

//TABS GENRALES DEL SITIO****************************************************

      $('#tab-container').easytabs();
      $('#tab-draclown').easytabs();

//SLIDER PARA PQRS

      var total_listas = $("#scroll_menu_pqrs").children().length;
      var listas_visibles = 5;

      if (total_listas <= 0){
        $('#scroll_btn_right').addClass('ocultar_flechas');
        $('#scroll_btn_left').addClass('ocultar_flechas');
      }

      $("#scroll_btn_right").click(function(){

          if(listas_visibles < total_listas){
            listas_visibles = listas_visibles+1;
            $("#scroll_menu_pqrs").animate({"margin-left": "-=120px"},"slow");
            $("#scroll_btn_left").fadeIn('slow');
          }
          if(listas_visibles == total_listas){
            $(this).fadeOut('slow');
          }
      });

      $("#scroll_btn_left").click(function(){

        if(listas_visibles > 5){
          listas_visibles = listas_visibles-1
          $("#scroll_menu_pqrs").animate({"margin-left": "+=120px"},"slow");
          $("#scroll_btn_right").fadeIn('slow');
        }
          if(listas_visibles == 5){
            $(this).fadeOut('slow');
          }
      });

//OLVIDE CONTRASEÑA

    $('#olvideDatos').click(function(){
        $('#validarDatos').fadeOut(100, function(){
            $('#recuperarDatos').fadeIn(100);
        });
        return false
    });

    $('#volverIngreso').click(function(){
        $('#recuperarDatos').fadeOut(100, function(){
            $('#validarDatos').fadeIn(100);
        });
        return false
    });

//PASOS PQRS

    $('#ingresar_usuario').click(function(){
        $('#init_registro').fadeOut(100, function(){
            $('#validarDatos').fadeIn(100);
        });
        return false
    });

    $('#presentarSolicitud').click(function(){
        $('#pqrs_uno').fadeOut(100, function(){
            $('#pqrs_dos').fadeIn(100);
        });
        return false
    });

    $('#pasoUno').click(function(){
        $('#pqrs_dos').fadeOut(100, function(){
            $('#pqrs_uno').fadeIn(100);
        });
        return false
    });

    $('#pasoTres').click(function(){
        $('#pqrs_dos').fadeOut(100, function(){
            $('#pqrs_tres').fadeIn(100);
        });
        return false
    });

    $('#pasoDos').click(function(){
        $('#pqrs_tres').fadeOut(100, function(){
            $('#pqrs_dos').fadeIn(100);
        });
        return false
    });

    $('#pasoCuatro').click(function(){
        $('#pqrs_tres').fadeOut(100, function(){
            $('#pqrs_cuatro').fadeIn(100);
        });
        return false
    });

    $('#volverTres').click(function(){
        $('#pqrs_cuatro').fadeOut(100, function(){
            $('#pqrs_tres').fadeIn(100);
        });
        return false
    });

});


