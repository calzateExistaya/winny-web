$(document).ready(function($) {

//****************FUNCION GENERALES DEL SITE***********************

    $('#banner-fade').bjqs({
      height      : 422,
      width       : '100%',
      responsive  : true,
      automatic : true,
      showcontrols : true, // show next and prev controls
			centercontrols : true, // center controls verically
			nexttext : 'Next', // Text for 'next' button (can use HTML)
			prevtext : 'Prev', // Text for 'previous' button (can use HTML)
			showmarkers : false, // Show individual slide markers
			centermarkers : false, // Center markers horizontally
    });


//FUNCION PARA DESPLEGAR MI MUNDO WINNY**************************************

  $.fn.slideFadeToggle  = function(speed, easing, callback) {
        return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
  };
  $('#desplegar').click(function (){
    $("#site_map").slideToggle(400);
  });

  $('#desplegar').click(function() {
    $("html, body").animate({ scrollTop: $(document).height() }, "slow");
    return false;
  });

  $('#carouselTools').bxSlider({
            minSlides: 4,
            maxSlides   : 4,
            slideWidth  : 125,
            moveSlides  : 1,
            infiniteLoop: false
        });

  // Funcionalidad del menú de productos en el home
  $('.navegacion .contenido_gnrl .menu_principal .item06').mouseenter(function(){
      $('.navegacion .contenido_gnrl .menu_principal .item06 a div.textos_nav').hide();
      $('nav ul.menu_principal li div.submenu-prod').fadeIn('300');
  }).mouseleave(function(){
      $('nav ul.menu_principal li div.submenu-prod').hide();
      $('.navegacion .contenido_gnrl .menu_principal .item06 a div.textos_nav').fadeIn('300');
  });

  // Funcionalidad del formulario de login

  var loginShow=0;
  $('#singin_btn').click(function(){
    if(loginShow == 0)
    {
      $('#loginForm').animate( { top: "0px" }, "slow");
      loginShow=1;
      $('#singin_btn').removeClass("open");
      $('#singin_btn').addClass("closed");
    }else
    {
      $('#loginForm').animate( { top: "-193px" }, "slow");
      loginShow=0;
      $('#singin_btn').removeClass("closed");
      $('#singin_btn').addClass("open");
    }
  });

//MEGA MENÃš PRINCIPAL**************************************************************

    $(function () {
        var $oe_menu = $('#oe_menu');
        var $oe_menu_items = $oe_menu.children('li');
        var $oe_overlay = $('#oe_overlay');
        var slider = $('#bxsliderMenu1').bxSlider({
            infiniteLoop: false,
            minSlides: 6,
            maxSlides: 6,
            slideWidth: 170,
            slideMargin: 10
        });
        var slider2 = $('#bxsliderMenu2').bxSlider({
            infiniteLoop: false,
            minSlides: 6,
            maxSlides: 6,
            slideWidth: 170,
            slideMargin: 10
        });
        var slider3 = $('#bxsliderMenu3').bxSlider({
            infiniteLoop: false,
            minSlides: 6,
            maxSlides: 6,
            slideWidth: 170,
            slideMargin: 10
        });
        var slider4 = $('#bxsliderMenu4').bxSlider({
            infiniteLoop: false,
            minSlides: 6,
            maxSlides: 6,
            slideWidth: 170,
            slideMargin: 10
        });
        var slider5 = $('#bxsliderMenu5').bxSlider({
            infiniteLoop: false,
            minSlides: 6,
            maxSlides: 6,
            slideWidth: 170,
            slideMargin: 10
        });

        $oe_menu_items.bind('mouseenter', function () {
            var $this = $(this);
            slider.reloadSlider();
            slider2.reloadSlider();
            slider3.reloadSlider();
            slider4.reloadSlider();
            slider5.reloadSlider();
            $this.addClass('slided selected');
            $this.children('div').css('z-index', '9999').stop(true, true).slideDown(200, function () {
                $oe_menu_items.not('.slided').children('div').hide();
                $this.removeClass('slided');
            });
        }).bind('mouseleave', function () {
            var $this = $(this);
            $this.removeClass('selected').children('div').css('z-index', '1');
        });

        $oe_menu.bind('mouseenter', function () {
            var $this = $(this);
            $oe_overlay.stop(true, true).fadeTo(200, 0.6);
            $this.addClass('hovered');
        }).bind('mouseleave', function () {
            var $this = $(this);
            $this.removeClass('hovered');
            $oe_overlay.stop(true, true).fadeTo(200, 0);
            $oe_menu_items.children('div').hide();
        })
    });

//FUNCION DATAPICKER

  $.datepicker.regional['es'] = {
    closeText: 'Fermer',
    prevText: 'Previo',
    nextText: 'Próximo',
    monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
    'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
    monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
    'Jul','Ago','Sep','Oct','Nov','Dic'],
    monthStatus: 'Voir un autre mois', yearStatus: 'Voir un autre année',
    dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
    dayNamesShort: ['Dom','Lun','Mar','Mie','Jue','Vie','Sáb'],
    dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sa'],
    dateFormat: 'dd/mm/yy', firstDay: 0,
    initStatus: 'Selecciona la fecha', isRTL: false};

  $.datepicker.setDefaults($.datepicker.regional['es']);

  $( "#datepicker" ).datepicker({
    changeMonth: true,
    changeYear: true,
    autoSize: true,
    yearRange: "1950:1995"
  });

  $( "#datepickerdos" ).datepicker({
    changeMonth: true,
    changeYear: true,
    autoSize: true,
    yearRange: "1950:1995"
  });

  $( ".fechanacimientobebe" ).datepicker({
    changeMonth: true,
    changeYear: true,
    autoSize: true,
    yearRange: "1950:1995"
  });

  $( ".fechaposibleparto" ).datepicker({
    changeMonth: true,
    changeYear: true,
    autoSize: true,
    yearRange: "1950:1995"
  });

//FUNCIONALIDAD TOGGLE

  $('#ingresar_bebe, #ingresar-datos-pareja').click(function(){
      $("#show_form, #show-datos-pareja").slideToggle('fast');
      return false
  });

$('#closedActualizarPass, #actualizarPssBtn').click(function(){
  $('.actualizarPass').slideToggle('fast');
  return false;
});

//FUNCION PARA PLACEHOLDER IE9-

  function add() {
      if($(this).val() === ''){
          $(this).val($(this).attr('placeholder')).addClass('placeholder');
      }
  }

  function remove() {
      if($(this).val() === $(this).attr('placeholder')){
          $(this).val('').removeClass('placeholder');
      }
  }

  // Create a dummy element for feature detection
  if (!('placeholder' in $('<input>')[0])) {

      // Select the elements that have a placeholder attribute
      $('input[placeholder], textarea[placeholder]').blur(add).focus(remove).each(add);

      // Remove the placeholder text before the form is submitted
      $('form').submit(function(){
          $(this).find('input[placeholder], textarea[placeholder]').each(remove);
      });
  }

  $('#form-olvido').click(function () {
    $('#form-ingreso').slideUp();
    $('#form-contrasena').slideDown();
  });

  $('#form-regresar').click(function () {
    $('#form-contrasena').slideUp();
    $('#form-ingreso').slideDown();
  });

});

