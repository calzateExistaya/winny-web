function scrollSlider ( to ) {
		$("section.mwcontetnSlider").mCustomScrollbar("scrollTo", to );
	};

$(document).ready(function($) {


	$("section.mwcontetnSlider").mCustomScrollbar({
	    horizontalScroll:true,
	    autoDraggerLength: true,
	    autoHideScrollbar: true,
	    contentTouchScroll: true,
	    theme:"dark"
	});

	$('#contentIngreso').easytabs();

	//MOSTRAR SECCION OLVIDE CONTRASEÑA

    $('#ingresar_usuario').click(function(){
		    $('#init_registro').fadeOut(100, function(){
		        $('#registro_form').fadeIn(100);
		    });
		    return false
	});

    $('#volver_init').click(function(){
		    $('#registro_form').fadeOut(100, function(){
		        $('#init_registro').fadeIn(100);
		    });
		    return false
	});

    $('#olvide_contrasena').click(function(){
		    $('#registro_form').fadeOut(100, function(){
		        $('#olvide_form').fadeIn(100);
		    });
		    return false
	});

    $('#cancelar_contrasena').click(function(){
		    $('#olvide_form').fadeOut(100, function(){
		        $('#registro_form').fadeIn(100);
		    });
		    return false
	});

});